# frozen_string_literal: true

require_relative 'spec_helper'
require 'coordinate'

RSpec.describe Coordinate do
  describe '#initialize' do
    subject(:coordinate) { described_class.new(x, y) }

    let(:x) { 0 }
    let(:y) { 1 }

    it 'stores x value' do
      expect(coordinate.x).to eq x
    end

    it 'stores y value' do
      expect(coordinate.y).to eq y
    end

    context 'when x is not an Integer' do
      let(:x) { 0.2 }

      it 'raises invalid argument exception' do
        expect { coordinate }.to raise_error(InvalidArgumentException, "x must be integer but given #{x}")
      end
    end

    context 'when y is not an Integer' do
      let(:y) { 0.2 }

      it 'raises invalid argument exception' do
        expect { coordinate }.to raise_error(InvalidArgumentException, "y must be integer but given #{y}")
      end
    end
  end

  describe '#next' do
    subject(:next_coordinate) { original_coordinate.next(direction) }

    let(:original_coordinate) { described_class.new(1, 2) }
    let(:direction) { instance_double(Direction, facing: Direction::NORTH) }

    it 'returns a coordinate' do
      expect(next_coordinate).to be_a described_class
    end

    it 'creates new instance of coordinate' do
      expect(next_coordinate).not_to be original_coordinate
    end

    context 'when direction is NORTH' do
      let(:direction) { instance_double(Direction, facing: Direction::NORTH) }

      it 'does NOT change coordinate x' do
        expect(next_coordinate.x).to eq original_coordinate.x
      end

      it 'increases coordinate y by 1' do
        expect(next_coordinate.y).to eq(original_coordinate.y + 1)
      end
    end

    context 'when direction is EAST' do
      let(:direction) { instance_double(Direction, facing: Direction::EAST) }

      it 'increases coordinate x by 1' do
        expect(next_coordinate.x).to eq(original_coordinate.x + 1)
      end

      it 'does NOT change coordinate y' do
        expect(next_coordinate.y).to eq original_coordinate.y
      end
    end

    context 'when direction is SOUTH' do
      let(:direction) { instance_double(Direction, facing: Direction::SOUTH) }

      it 'does NOT change coordinate x' do
        expect(next_coordinate.x).to eq original_coordinate.x
      end

      it 'decreases coordinate y by 1' do
        expect(next_coordinate.y).to eq(original_coordinate.y - 1)
      end
    end

    context 'when direction is WEST' do
      let(:direction) { instance_double(Direction, facing: Direction::WEST) }

      it 'decreases coordinate x by 1' do
        expect(next_coordinate.x).to eq(original_coordinate.x - 1)
      end

      it 'does NOT change coordinate y' do
        expect(next_coordinate.y).to eq original_coordinate.y
      end
    end
  end
end
