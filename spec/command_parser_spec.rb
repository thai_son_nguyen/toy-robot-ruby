# frozen_string_literal: true

require_relative 'spec_helper'
require 'command_parser'

RSpec.describe CommandParser do
  describe '#parse' do
    subject(:parse) { described_class.parse(command_string) }

    let(:command_string) { 'aa' }
    let(:left_command) { instance_double(Commands::Left) }
    let(:right_command) { instance_double(Commands::Right) }
    let(:move_command) { instance_double(Commands::Move) }
    let(:report_command) { instance_double(Commands::Report) }
    let(:place_command) { instance_double(Commands::Place) }

    before do
      allow(Commands::Left).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::Right).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::Move).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::Report).to receive(:try_parse).with(command_string).and_return(nil)
      allow(Commands::Place).to receive(:try_parse).with(command_string).and_return(nil)
    end

    context 'when command is valid left command' do
      before do
        allow(Commands::Left).to receive(:try_parse).with(command_string).and_return(left_command)
      end

      it 'returns the left command' do
        expect(parse).to be left_command
      end
    end

    context 'when command is valid right command' do
      before do
        allow(Commands::Right).to receive(:try_parse).with(command_string).and_return(right_command)
      end

      it 'returns the right command' do
        expect(parse).to be right_command
      end
    end

    context 'when command is valid move command' do
      before do
        allow(Commands::Move).to receive(:try_parse).with(command_string).and_return(move_command)
      end

      it 'returns the move command' do
        expect(parse).to be move_command
      end
    end

    context 'when command is valid place command' do
      before do
        allow(Commands::Place).to receive(:try_parse).with(command_string).and_return(place_command)
      end

      it 'returns the place command' do
        expect(parse).to be place_command
      end
    end

    context 'when command is valid report command' do
      before do
        allow(Commands::Report).to receive(:try_parse).with(command_string).and_return(report_command)
      end

      it 'returns the report command' do
        expect(parse).to be report_command
      end
    end

    context 'when command is not a valid one' do
      let(:null_command) { instance_double(Commands::Null) }

      before do
        allow(Commands::Null).to receive(:new).with(command_string).and_return(null_command)
      end

      it 'returns null command' do
        expect(parse).to be null_command
      end
    end

    context 'when there is exception parsing command' do
      let(:null_command) { instance_double(Commands::Null) }

      before do
        allow(Commands::Left).to receive(:try_parse).and_raise(StandardError)
        allow(Commands::Null).to receive(:new).with(command_string).and_return(null_command)
      end

      it 'returns null command' do
        expect(parse).to be null_command
      end
    end
  end
end
