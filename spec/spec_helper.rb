# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

RSpec.configure do |config|
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.formatter = :documentation
end

require 'byebug'
