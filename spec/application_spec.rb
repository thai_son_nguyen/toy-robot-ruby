# frozen_string_literal: true

require_relative 'spec_helper'
require 'application'
require 'commands/base_command'
require 'robot'
require 'table'

RSpec.describe Application do
  describe '#run' do
    subject(:run) { described_class.run }

    let(:table) { instance_double(Table) }
    let(:robot) { instance_double(Robot) }
    let(:welcome_message) do
      <<~MSG
        Welcome to robot simulator !
        1. PLACE X,Y,F -> To place robot and start (with F = NORTH|SOUTH|WEST|EAST).
        2. MOVE -> To move robot forward.
        3. LEFT -> To turn left.
        4. RIGHT -> To turn right.
        5. REPORT -> To report location.
        6. EXIT or empty line -> To quit program.
      MSG
    end
    let(:gets_result) { double }
    let(:first_command_string) { 'a' }
    let(:exit_command_string) { 'EXIT' }
    let(:command) { instance_double(Commands::BaseCommand) }

    before do
      allow(Table).to receive(:new).and_return(table)
      allow(Robot).to receive(:new).and_return(robot)
      allow(described_class).to receive(:puts)
      allow(described_class).to receive(:gets).and_return(gets_result)
      allow(gets_result).to receive(:chomp).and_return(first_command_string, exit_command_string)
      allow(CommandParser).to receive(:parse).and_return(command)
      allow(command).to receive(:execute)
    end

    it 'creates a 5 x 5 table' do
      expect(Table).to receive(:new).with(5, 5).once
      run
    end

    it 'creates a robot with the table' do
      expect(Robot).to receive(:new).with(table).once
      run
    end

    it 'outputs welcome message' do
      expect(described_class).to receive(:puts).with(welcome_message).once
      run
    end

    it 'asks for input' do
      expect(described_class).to receive(:gets)
      run
    end

    it 'parses command' do
      expect(CommandParser).to receive(:parse).with(first_command_string)
      run
    end

    it 'executes command' do
      expect(command).to receive(:execute).with(robot).once
      run
    end

    context 'when there are more than 1 command' do
      let(:second_command_string) { 'ab' }
      let(:second_command) { instance_double(Commands::BaseCommand) }

      before do
        allow(gets_result).to receive(:chomp).and_return(
          first_command_string, second_command_string, exit_command_string
        )
        allow(CommandParser).to receive(:parse).with(first_command_string).and_return(command)
        allow(CommandParser).to receive(:parse).with(second_command_string).and_return(second_command)
        allow(command).to receive(:execute)
        allow(second_command).to receive(:execute)
      end

      it 'executes first command' do
        expect(command).to receive(:execute).with(robot).once
        run
      end

      it 'executes second command' do
        expect(second_command).to receive(:execute).with(robot).once
        run
      end
    end
  end
end
