# frozen_string_literal: true

require_relative '../spec_helper'
require 'robot'
require 'table'
require 'coordinate'
require 'direction'
require 'command_parser'
require 'commands/null'

RSpec.describe 'Null' do
  subject(:null) { command.execute(robot) }

  let(:command) { CommandParser.parse('abc') }
  let(:robot) { Robot.new(Table.new(5, 5)) }
  let(:original_coordinate) { Coordinate.new(2, 2) }
  let(:original_direction) { Direction.new(Direction::NORTH) }

  before do
    robot.move_to(original_coordinate, original_direction)
  end

  it 'does NOT touch robot' do
    expect(robot.coordinate).to be original_coordinate
    expect(robot.direction).to be original_direction
  end
end
