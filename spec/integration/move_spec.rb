# frozen_string_literal: true

require_relative '../spec_helper'
require 'robot'
require 'table'
require 'coordinate'
require 'direction'
require 'command_parser'
require 'commands/move'

RSpec.describe 'Move' do
  subject(:move) { command.execute(robot) }

  let(:command) { CommandParser.parse('MOVE') }
  let(:robot) { Robot.new(Table.new(5, 5)) }
  let(:original_coordinate) { Coordinate.new(2, 2) }

  context 'when direction is north' do
    let(:original_direction) { Direction.new(Direction::NORTH) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'moves robot 1 step forward north' do
      move
      expect(robot.coordinate.x).to eq(original_coordinate.x)
      expect(robot.coordinate.y).to eq(original_coordinate.y + 1)
      expect(robot.direction).to eq(original_direction)
    end
  end

  context 'when direction is east' do
    let(:original_direction) { Direction.new(Direction::EAST) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'moves robot 1 step forward east' do
      move
      expect(robot.coordinate.x).to eq(original_coordinate.x + 1)
      expect(robot.coordinate.y).to eq(original_coordinate.y)
      expect(robot.direction).to eq(original_direction)
    end
  end

  context 'when direction is south' do
    let(:original_direction) { Direction.new(Direction::SOUTH) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'moves robot 1 step forward south' do
      move
      expect(robot.coordinate.x).to eq(original_coordinate.x)
      expect(robot.coordinate.y).to eq(original_coordinate.y - 1)
      expect(robot.direction).to eq(original_direction)
    end
  end

  context 'when direction is west' do
    let(:original_direction) { Direction.new(Direction::WEST) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'moves robot 1 step forward west' do
      move
      expect(robot.coordinate.x).to eq(original_coordinate.x - 1)
      expect(robot.coordinate.y).to eq(original_coordinate.y)
      expect(robot.direction).to eq(original_direction)
    end
  end

  context 'when next coordinate is not inbound' do
    let(:original_coordinate) { Coordinate.new(0, 2) }
    let(:original_direction) { Direction.new(Direction::WEST) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'does NOT move' do
      move
      expect(robot.coordinate.x).to eq(original_coordinate.x)
      expect(robot.coordinate.y).to eq(original_coordinate.y)
      expect(robot.direction).to eq(original_direction)
    end
  end

  context 'when robot has not been placed' do
    it 'does NOT move' do
      move
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end
end
