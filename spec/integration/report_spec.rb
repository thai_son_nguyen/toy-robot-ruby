# frozen_string_literal: true

require_relative '../spec_helper'
require 'robot'
require 'table'
require 'coordinate'
require 'direction'
require 'command_parser'
require 'commands/report'

RSpec.describe 'Report' do
  subject(:report) { command.execute(robot) }

  let(:command) { CommandParser.parse('REPORT') }
  let(:robot) { Robot.new(Table.new(5, 5)) }

  context 'when robot has been placed' do
    let(:direction) { Direction.new(Direction::NORTH) }
    let(:coordinate) { Coordinate.new(2, 2) }
    let(:expect_report) do
      "Position: #{coordinate.x}, #{coordinate.y}, #{direction.facing.to_s.upcase}"
    end

    before do
      robot.move_to(coordinate, direction)
    end

    it 'reports robot location' do
      expect(STDOUT).to receive(:puts).with(expect_report).once
      report
    end
  end

  context 'when robot has not been placed' do
    it 'says that robot is not ready' do
      expect(STDOUT).to receive(:puts).with('Not ready')
      report
    end
  end
end
