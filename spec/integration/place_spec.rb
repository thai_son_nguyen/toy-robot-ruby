# frozen_string_literal: true

require_relative '../spec_helper'
require 'robot'
require 'table'
require 'coordinate'
require 'direction'
require 'command_parser'
require 'commands/place'

RSpec.describe 'Place' do
  subject(:place) { command.execute(robot) }

  let(:command) { CommandParser.parse("PLACE #{coordinate.x},#{coordinate.y},#{direction.facing.to_s.upcase}") }
  let(:robot) { Robot.new(Table.new(5, 5)) }
  let(:direction) { Direction.new(Direction::NORTH) }

  context 'when place at bottom left' do
    let(:coordinate) { Coordinate.new(0, 0) }

    it 'places robot at the location' do
      place
      expect(robot.coordinate.x).to eq coordinate.x
      expect(robot.coordinate.y).to eq coordinate.y
      expect(robot.direction.facing).to eq direction.facing
    end
  end

  context 'when place at bottom right' do
    let(:coordinate) { Coordinate.new(4, 0) }

    it 'places robot at the location' do
      place
      expect(robot.coordinate.x).to eq coordinate.x
      expect(robot.coordinate.y).to eq coordinate.y
      expect(robot.direction.facing).to eq direction.facing
    end
  end

  context 'when place at top left' do
    let(:coordinate) { Coordinate.new(0, 4) }

    it 'places robot at the location' do
      place
      expect(robot.coordinate.x).to eq coordinate.x
      expect(robot.coordinate.y).to eq coordinate.y
      expect(robot.direction.facing).to eq direction.facing
    end
  end

  context 'when place at top right' do
    let(:coordinate) { Coordinate.new(4, 4) }

    it 'places robot at the location' do
      place
      expect(robot.coordinate.x).to eq coordinate.x
      expect(robot.coordinate.y).to eq coordinate.y
      expect(robot.direction.facing).to eq direction.facing
    end
  end

  context 'when place in the middle' do
    let(:coordinate) { Coordinate.new(2, 3) }

    it 'places robot at the location' do
      place
      expect(robot.coordinate.x).to eq coordinate.x
      expect(robot.coordinate.y).to eq coordinate.y
      expect(robot.direction.facing).to eq direction.facing
    end
  end

  context 'when place outbound min x' do
    let(:coordinate) { Coordinate.new(-1, 1) }

    it 'does NOT place robot at the location' do
      place
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end

  context 'when place outbound max x' do
    let(:coordinate) { Coordinate.new(5, 1) }

    it 'does NOT place robot at the location' do
      place
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end

  context 'when place outbound min y' do
    let(:coordinate) { Coordinate.new(1, -1) }

    it 'does NOT place robot at the location' do
      place
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end

  context 'when place outbound max y' do
    let(:coordinate) { Coordinate.new(1, 5) }

    it 'does NOT place robot at the location' do
      place
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end

  context 'when place outbound' do
    let(:original_coordinate) { Coordinate.new(1, 1) }
    let(:original_direction) { Direction.new(Direction::WEST) }
    let(:coordinate) { Coordinate.new(1, 5) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'does NOT update robot location' do
      place
      expect(robot.coordinate).to be original_coordinate
      expect(robot.direction).to be original_direction
    end
  end
end
