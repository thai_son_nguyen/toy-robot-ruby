# frozen_string_literal: true

require_relative '../spec_helper'
require 'robot'
require 'table'
require 'coordinate'
require 'direction'
require 'command_parser'
require 'commands/left'

RSpec.describe 'Left' do
  subject(:left) { command.execute(robot) }

  let(:command) { CommandParser.parse('LEFT') }
  let(:robot) { Robot.new(Table.new(5, 5)) }
  let(:original_coordinate) { Coordinate.new(2, 2) }

  context 'when direction is north' do
    let(:original_direction) { Direction.new(Direction::NORTH) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'turns robot to west' do
      left
      expect(robot.coordinate).to be original_coordinate
      expect(robot.direction.facing).to eq(Direction::WEST)
    end
  end

  context 'when direction is east' do
    let(:original_direction) { Direction.new(Direction::EAST) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'turns robot to north' do
      left
      expect(robot.coordinate).to be original_coordinate
      expect(robot.direction.facing).to eq(Direction::NORTH)
    end
  end

  context 'when direction is south' do
    let(:original_direction) { Direction.new(Direction::SOUTH) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'turns robot to east' do
      left
      expect(robot.coordinate).to be original_coordinate
      expect(robot.direction.facing).to eq(Direction::EAST)
    end
  end

  context 'when direction is west' do
    let(:original_direction) { Direction.new(Direction::WEST) }

    before do
      robot.move_to(original_coordinate, original_direction)
    end

    it 'turns robot to south' do
      left
      expect(robot.coordinate).to be original_coordinate
      expect(robot.direction.facing).to eq(Direction::SOUTH)
    end
  end

  context 'when robot has not been placed' do
    it 'does NOT turn' do
      left
      expect(robot.coordinate).to be_nil
      expect(robot.direction).to be_nil
    end
  end
end
