# frozen_string_literal: true

require_relative 'spec_helper'
require 'robot'
require 'table'
require 'coordinate'

RSpec.describe Robot do
  describe '#initialize' do
    subject(:robot) { described_class.new(table) }

    let(:table) { instance_double(Table) }

    it 'stores the table' do
      expect(robot.table).to eq table
    end

    it 'sets coordinate as nil' do
      expect(robot.coordinate).to be_nil
    end

    it 'sets direction as nil' do
      expect(robot.direction).to be_nil
    end
  end

  describe '#move_to' do
    subject(:move_to) { robot.move_to(coordinate, direction) }

    let(:robot) { described_class.new(table) }
    let(:table) { instance_double(Table) }
    let(:coordinate) { instance_double(Coordinate) }
    let(:direction) { instance_double(Direction) }

    before do
      allow(table).to receive(:occupiable?).and_return(true)
    end

    it 'calls table to determin whether a coordinate is occupiable' do
      expect(table).to receive(:occupiable?).with(coordinate).once
      move_to
    end

    context 'when coordinate is occupiable' do
      it 'updates coordinate' do
        move_to
        expect(robot.coordinate).to be coordinate
      end

      it 'updates direction' do
        move_to
        expect(robot.direction).to be direction
      end
    end

    context 'when coordinate is not occupiable' do
      before do
        allow(table).to receive(:occupiable?).and_return(false)
      end

      it 'does NOT update coordinate' do
        expect { move_to }.not_to change(robot, :coordinate)
      end

      it 'does NOT update direction' do
        expect { move_to }.not_to change(robot, :direction)
      end
    end
  end

  describe '#on_table?' do
    subject(:robot) { described_class.new(table) }

    let(:table) { instance_double(Table) }

    context 'when robot has been moved on table' do
      let(:coordinate) { instance_double(Coordinate) }
      let(:direction) { instance_double(Direction) }

      before do
        allow(table).to receive(:occupiable?).and_return(true)
        robot.move_to(coordinate, direction)
      end

      it 'returns true' do
        expect(robot.on_table?).to be true
      end
    end

    context 'when robot is not on table' do
      it 'returns false' do
        expect(robot.on_table?).to be false
      end
    end
  end
end
