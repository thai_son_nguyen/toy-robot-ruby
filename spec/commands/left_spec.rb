# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/left'
require 'robot'
require 'direction'

RSpec.describe Commands::Left do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { 'LEFT' }

    it 'creates an instance of left command' do
      expect(try_parse).to be_an_instance_of described_class
    end

    context 'when command is not a left command' do
      let(:command_string) { 'LEFT ' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#execute' do
    subject(:execute) { described_class.new.execute(robot) }

    let(:robot) do
      instance_double(Robot, on_table?: true, coordinate: current_coordinate, direction: current_direction)
    end
    let(:current_coordinate) { instance_double(Coordinate) }
    let(:current_direction) { instance_double(Direction, left: new_direction) }
    let(:new_direction) { instance_double(Direction) }

    before do
      allow(robot).to receive(:move_to)
    end

    it 'asks robot to rotate left once' do
      expect(robot).to receive(:move_to).with(current_coordinate, new_direction).once
      execute
    end

    context 'when robot is not set' do
      let(:robot) { nil }

      it 'does NOT ask to rotate left' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end

    context 'when robot is not on table' do
      before do
        allow(robot).to receive(:on_table?).and_return(false)
      end

      it 'does NOT ask robot to rotate left' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end
  end
end
