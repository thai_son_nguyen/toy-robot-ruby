# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/report'
require 'robot'
require 'coordinate'
require 'direction'

RSpec.describe Commands::Report do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { 'REPORT' }

    it 'creates an instance of REPORT command' do
      expect(try_parse).to be_an_instance_of described_class
    end

    context 'when command is not a REPORT command' do
      let(:command_string) { 'REPORT ' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#execute' do
    subject(:execute) { described_class.new.execute(robot) }

    let(:robot) { instance_double(Robot, coordinate: coordinate, direction: direction, on_table?: true) }
    let(:coordinate) { instance_double(Coordinate, x: 1, y: 2) }
    let(:direction) { instance_double(Direction, facing: Direction::NORTH) }
    let(:expect_report) do
      "Position: #{coordinate.x}, #{coordinate.y}, #{direction.facing.to_s.upcase}"
    end

    it 'gets robot coordinate' do
      expect(robot).to receive(:coordinate).once
      execute
    end

    it 'gets robot direction' do
      expect(robot).to receive(:direction).once
      execute
    end

    it 'outputs robot coordinate and direction' do
      expect(STDOUT).to receive(:puts).with(expect_report).once
      execute
    end

    context 'when robot is not set' do
      let(:robot) { nil }

      it 'says robot is not ready' do
        expect(STDOUT).not_to receive(:puts).with('Not ready').once
        execute
      end
    end

    context 'when robot is not on table' do
      before do
        allow(robot).to receive(:on_table?).and_return(false)
      end

      it 'says robot is not ready' do
        expect(STDOUT).not_to receive(:puts).with('Not ready').once
        execute
      end
    end
  end
end
