# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/base_command'

RSpec.describe Commands::BaseCommand do
  describe '#execute' do
    subject(:execute) { described_class.new.execute(robot) }

    let(:robot) { instance_double(Robot) }

    it 'raises not implemented exception' do
      expect { execute }.to raise_error NotImplementedError, 'Excute is not implemented'
    end
  end
end
