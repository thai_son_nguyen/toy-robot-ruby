# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/move'
require 'robot'
require 'coordinate'
require 'direction'

RSpec.describe Commands::Move do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { 'MOVE' }

    it 'creates an instance of MOVE command' do
      expect(try_parse).to be_an_instance_of described_class
    end

    context 'when command is not a MOVE command' do
      let(:command_string) { 'MOVE ' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#execute' do
    subject(:execute) { described_class.new.execute(robot) }

    let(:robot) do
      instance_double(Robot, on_table?: true, coordinate: current_coordinate, direction: current_direction)
    end
    let(:current_coordinate) { instance_double(Coordinate) }
    let(:new_coordinate) { instance_double(Coordinate) }
    let(:current_direction) { instance_double(Direction) }

    before do
      allow(robot).to receive(:move_to)
      allow(current_coordinate).to receive(:next).with(current_direction).and_return(new_coordinate)
    end

    it 'asks robot to move forward once' do
      expect(robot).to receive(:move_to).with(new_coordinate, current_direction).once
      execute
    end

    context 'when robot is not set' do
      let(:robot) { nil }

      it 'does NOT ask to move' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end

    context 'when robot is not on table' do
      before do
        allow(robot).to receive(:on_table?).and_return(false)
      end

      it 'does NOT ask robot to move' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end
  end
end
