# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/place'
require 'robot'
require 'coordinate'
require 'direction'

RSpec.describe Commands::Place do
  describe '#try_parse' do
    subject(:try_parse) { described_class.try_parse(command_string) }

    let(:command_string) { "PLACE #{coordinate_x},#{coordinate_y},#{direction_string}" }
    let(:coordinate_x) { 1 }
    let(:coordinate_y) { 2 }
    let(:direction_string) { 'NORTH' }
    let(:coordinate) { instance_double(Coordinate) }
    let(:direction) { instance_double(Direction) }
    let(:expected_command) { instance_double(described_class) }

    before do
      allow(Coordinate).to receive(:new).with(coordinate_x, coordinate_y).and_return(coordinate)
      allow(Direction).to receive(:new).with(Direction::NORTH).and_return(direction)
      allow(described_class).to receive(:new).with(coordinate, direction).and_return(expected_command)
    end

    it 'creates a place command' do
      expect(try_parse).to be expected_command
    end

    context 'when command does NOT start with PLACE' do
      let(:command_string) { "PLACES #{coordinate_x},#{coordinate_y}, #{direction_string}" }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end

    context 'when coordinate is invalid' do
      let(:coordinate_x) { 'a' }
      let(:coordinate_y) { 2 }
      let(:direction_string) { 'NORTH' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end

    context 'when direction is not valid' do
      let(:coordinate_x) { 1 }
      let(:coordinate_y) { 2 }
      let(:direction_string) { 'NOR' }

      it 'returns nil' do
        expect(try_parse).to be_nil
      end
    end
  end

  describe '#initialize' do
    subject(:place_command) { described_class.new(coordinate, direction) }

    let(:coordinate) { instance_double(Coordinate) }
    let(:direction) { instance_double(Direction) }

    it 'stores coordinate' do
      expect(place_command.coordinate).to be coordinate
    end

    it 'stores direction' do
      expect(place_command.direction).to be direction
    end
  end

  describe '#execute' do
    subject(:execute) { command.execute(robot) }

    let(:coordinate) { instance_double(Coordinate) }
    let(:direction) { instance_double(Direction) }
    let(:command) { described_class.new(coordinate, direction) }
    let(:robot) { instance_double(Robot) }

    before do
      allow(robot).to receive(:move_to)
    end

    it 'places robot' do
      expect(robot).to receive(:move_to).with(coordinate, direction).once
      execute
    end

    context 'when robot is not set' do
      let(:robot) { nil }

      it 'does NOT place robot' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end

    context 'when coordinate is not set' do
      let(:coordinate) { nil }

      it 'does NOT place robot' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end

    context 'when direction is not set' do
      let(:direction) { nil }

      it 'does NOT place robot' do
        expect(robot).not_to receive(:move_to)
        execute
      end
    end
  end
end
