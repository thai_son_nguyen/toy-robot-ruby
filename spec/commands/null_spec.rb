# frozen_string_literal: true

require_relative '../spec_helper'
require 'commands/null'

RSpec.describe Commands::Null do
  describe '#execute' do
    subject(:execute) { described_class.new(command_string).execute(robot) }

    let(:command_string) { 'abc' }
    let(:robot) { instance_double(Robot) }

    it 'outputs error message' do
      expect(STDOUT).to receive(:puts).with("Unable to execute command: #{command_string}").once
      execute
    end
  end
end
