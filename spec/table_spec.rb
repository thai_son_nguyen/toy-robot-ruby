# frozen_string_literal: true

require_relative 'spec_helper'
require 'table'
require 'coordinate'

RSpec.describe Table do
  describe '#initialize' do
    subject(:table) { described_class.new(width, height) }

    let(:width) { 4 }
    let(:height) { 4 }

    it 'stores width value' do
      expect(table.width).to eq width
    end

    it 'stores height value' do
      expect(table.height).to eq height
    end

    context 'when width is not an Integer' do
      let(:width) { 0.2 }

      it 'raises invalid argument exception' do
        expect { table }.to raise_error(
          InvalidArgumentException, "width must be positive integer but given #{width}"
        )
      end
    end

    context 'when width is 0' do
      let(:width) { 0 }

      it 'raises invalid argument exception' do
        expect { table }.to raise_error(
          InvalidArgumentException, "width must be positive integer but given #{width}"
        )
      end
    end

    context 'when height is not an Integer' do
      let(:height) { 0.2 }

      it 'raises invalid argument exception' do
        expect { table }.to raise_error(
          InvalidArgumentException, "height must be positive integer but given #{height}"
        )
      end
    end

    context 'when height is 0' do
      let(:height) { 0 }

      it 'raises invalid argument exception' do
        expect { table }.to raise_error(
          InvalidArgumentException, "height must be positive integer but given #{height}"
        )
      end
    end
  end

  describe '#occupiable?' do
    subject(:is_occupiable) { described_class.new(width, height).occupiable?(coordinate) }

    let(:width) { 4 }
    let(:height) { 4 }
    let(:coordinate) { instance_double(Coordinate, x: coordinate_x, y: coordinate_y) }

    context 'when the coordinate is bottom left' do
      let(:coordinate_x) { 0 }
      let(:coordinate_y) { 0 }

      it 'considers the coordinate as occupiable' do
        expect(is_occupiable).to be true
      end
    end

    context 'when the coordinate is bottom right' do
      let(:coordinate_x) { width - 1 }
      let(:coordinate_y) { 0 }

      it 'considers the coordinate as occupiable' do
        expect(is_occupiable).to be true
      end
    end

    context 'when the coordinate is top left' do
      let(:coordinate_x) { 0 }
      let(:coordinate_y) { height - 1 }

      it 'considers the coordinate as occupiable' do
        expect(is_occupiable).to be true
      end
    end

    context 'when the coordinate is top right' do
      let(:coordinate_x) { width - 1 }
      let(:coordinate_y) { height - 1 }

      it 'considers the coordinate as occupiable' do
        expect(is_occupiable).to be true
      end
    end

    context 'when the coordinate x is not inbound' do
      let(:coordinate_x) { width }
      let(:coordinate_y) { 0 }

      it 'considers the coordinate as not occupiable' do
        expect(is_occupiable).to be false
      end
    end

    context 'when the coordinate x is less than 0' do
      let(:coordinate_x) { -1 }
      let(:coordinate_y) { 0 }

      it 'considers the coordinate as not occupiable' do
        expect(is_occupiable).to be false
      end
    end

    context 'when the coordinate y is not inbound' do
      let(:coordinate_x) { 0 }
      let(:coordinate_y) { height }

      it 'considers the coordinate as not occupiable' do
        expect(is_occupiable).to be false
      end
    end

    context 'when the coordinate y is less than 0' do
      let(:coordinate_x) { 0 }
      let(:coordinate_y) { -1 }

      it 'considers the coordinate as not occupiable' do
        expect(is_occupiable).to be false
      end
    end

    context 'when coordinate is nil' do
      let(:coordinate) { nil }

      it 'considers the coordinate as not occupiable' do
        expect(is_occupiable).to be false
      end
    end
  end
end
