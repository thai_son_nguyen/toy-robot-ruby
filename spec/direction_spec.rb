# frozen_string_literal: true

require_relative 'spec_helper'
require 'direction'

RSpec.describe Direction do
  let(:facing) { :north }

  describe '#initialize' do
    subject(:direction) { described_class.new(facing) }

    it 'stores facing' do
      expect(direction.facing).to eq facing
    end

    [:north, :east, :south, :west].each do |given_facing|
      context "when given facing is #{given_facing}" do
        let(:facing) { given_facing }

        it 'considers given facing as valid' do
          expect(direction).to be_a described_class
        end
      end
    end

    context 'when facing is invalid' do
      let(:facing) { :abc }

      it 'raises InvalidArgumentException' do
        expect { direction }.to raise_error(InvalidArgumentException, 'Invalid facing')
      end
    end
  end

  describe '#left' do
    subject(:left) { original_direction.left }

    let(:original_direction) { described_class.new(facing) }

    it 'returns a direction' do
      expect(left).to be_a described_class
    end

    it 'creates new instance of direction' do
      expect(left).not_to be original_direction
    end

    context 'when facing north' do
      let(:facing) { :north }

      it 'changes facing to west' do
        expect(left.facing).to eq :west
      end
    end

    context 'when facing east' do
      let(:facing) { :east }

      it 'changes facing to north' do
        expect(left.facing).to eq :north
      end
    end

    context 'when facing south' do
      let(:facing) { :south }

      it 'changes facing to east' do
        expect(left.facing).to eq :east
      end
    end

    context 'when facing west' do
      let(:facing) { :west }

      it 'changes facing to south' do
        expect(left.facing).to eq :south
      end
    end
  end

  describe '#right' do
    subject(:right) { original_direction.right }

    let(:original_direction) { described_class.new(facing) }

    it 'returns a direction' do
      expect(right).to be_a described_class
    end

    it 'creates new instance of direction' do
      expect(right).not_to be original_direction
    end

    context 'when facing north' do
      let(:facing) { :north }

      it 'changes facing to east' do
        expect(right.facing).to eq :east
      end
    end

    context 'when facing east' do
      let(:facing) { :east }

      it 'changes facing to south' do
        expect(right.facing).to eq :south
      end
    end

    context 'when facing south' do
      let(:facing) { :south }

      it 'changes facing to west' do
        expect(right.facing).to eq :west
      end
    end

    context 'when facing west' do
      let(:facing) { :west }

      it 'changes facing to north' do
        expect(right.facing).to eq :north
      end
    end
  end
end
