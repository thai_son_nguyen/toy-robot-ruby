# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Report < BaseCommand
    class << self
      def try_parse(command_string)
        new if /^REPORT\z/i.match?(command_string)
      end
    end

    def execute(robot)
      if robot&.on_table?
        coordinate = robot.coordinate
        direction = robot.direction
        puts "Position: #{coordinate.x}, #{coordinate.y}, #{direction.facing.to_s.upcase}"
      else
        puts 'Not ready'
      end
    end
  end
end
