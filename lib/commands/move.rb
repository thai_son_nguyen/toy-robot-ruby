# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Move < BaseCommand
    class << self
      def try_parse(command_string)
        new if /^MOVE\z/i.match?(command_string)
      end
    end

    def execute(robot)
      return unless robot&.on_table?
      new_coordinate = robot.coordinate.next(robot.direction)
      robot.move_to(new_coordinate, robot.direction)
    end
  end
end
