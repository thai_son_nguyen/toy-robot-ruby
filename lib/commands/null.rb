# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Null < BaseCommand
    def initialize(command_string)
      @command_string = command_string
    end

    def execute(_robot)
      puts "Unable to execute command: #{@command_string}"
    end
  end
end
