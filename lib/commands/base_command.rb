# frozen_string_literal: true

module Commands
  class BaseCommand
    def execute(_robot)
      raise NotImplementedError, 'Excute is not implemented'
    end
  end
end
