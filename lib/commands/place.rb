# frozen_string_literal: true

require_relative 'base_command'
require_relative '../coordinate'
require_relative '../direction'

module Commands
  class Place < BaseCommand
    class << self
      def try_parse(command_string)
        create_command(command_string) if PATTERN.match?(command_string)
      end

      private

      PATTERN = /^PLACE (\d+),(\d+),(NORTH|EAST|SOUTH|WEST)\z/i

      def create_command(command_string)
        args = PATTERN.match(command_string)
        coordinate = Coordinate.new(args[1].to_i, args[2].to_i)
        direction = Direction.new(args[3].downcase.to_sym)
        new(coordinate, direction)
      end
    end

    attr_reader :coordinate, :direction

    def initialize(coordinate, direction)
      @coordinate = coordinate
      @direction = direction
    end

    def execute(robot)
      robot&.move_to(coordinate, direction) unless coordinate.nil? || direction.nil?
    end
  end
end
