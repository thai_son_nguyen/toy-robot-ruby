# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Right < BaseCommand
    class << self
      def try_parse(command_string)
        new if /^RIGHT\z/i.match?(command_string)
      end
    end

    def execute(robot)
      robot.move_to(robot.coordinate, robot.direction.right) if robot&.on_table?
    end
  end
end
