# frozen_string_literal: true

require_relative 'base_command'

module Commands
  class Left < BaseCommand
    class << self
      def try_parse(command_string)
        new if /^LEFT\z/i.match?(command_string)
      end
    end

    def execute(robot)
      robot.move_to(robot.coordinate, robot.direction.left) if robot&.on_table?
    end
  end
end
