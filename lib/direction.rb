# frozen_string_literal: true

require_relative 'invalid_argument_exception'

class Direction
  NORTH = :north
  EAST = :east
  SOUTH = :south
  WEST = :west

  attr_reader :facing

  def initialize(facing)
    raise InvalidArgumentException, 'Invalid facing' unless valid_facing?(facing)
    @facing = facing
  end

  def left
    new_facing = DIRECTIONS[DIRECTIONS.index(@facing) - 1]
    Direction.new(new_facing)
  end

  def right
    new_facing_index = (DIRECTIONS.index(@facing) + 1) % DIRECTIONS.length
    new_facing = DIRECTIONS[new_facing_index]
    Direction.new(new_facing)
  end

  private

  DIRECTIONS = [Direction::NORTH, Direction::EAST, Direction::SOUTH, Direction::WEST].freeze

  def valid_facing?(facing)
    DIRECTIONS.include? facing
  end
end
