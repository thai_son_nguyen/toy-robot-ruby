# frozen_string_literal: true

class InvalidArgumentException < StandardError
end
