# frozen_string_literal: true

require_relative 'commands/left'
require_relative 'commands/right'
require_relative 'commands/report'
require_relative 'commands/move'
require_relative 'commands/place'
require_relative 'commands/null'

class CommandParser
  class << self
    def parse(command_string)
      try_parse(command_string) || Commands::Null.new(command_string)
    rescue StandardError
      Commands::Null.new(command_string)
    end

    private

    def try_parse(command_string)
      supported_parsers.map { |parser| parser.try_parse(command_string) }
                       .find { |command| !command.nil? }
    end

    def supported_parsers
      [Commands::Left, Commands::Right, Commands::Move, Commands::Place, Commands::Report]
    end
  end
end
