# frozen_string_literal: true

require_relative 'invalid_argument_exception'
require_relative 'direction'

class Coordinate
  attr_reader :x, :y

  def initialize(coordinate_x, coordinate_y)
    guarantee_valid_coordinate_point('x', coordinate_x)
    guarantee_valid_coordinate_point('y', coordinate_y)
    @x = coordinate_x
    @y = coordinate_y
  end

  def next(direction)
    case direction.facing
    when Direction::NORTH
      Coordinate.new(@x, @y + 1)
    when Direction::EAST
      Coordinate.new(@x + 1, @y)
    when Direction::SOUTH
      Coordinate.new(@x, @y - 1)
    when Direction::WEST
      Coordinate.new(@x - 1, @y)
    end
  end

  private

  def guarantee_valid_coordinate_point(name, value)
    valid = (value.is_a? Integer)
    raise InvalidArgumentException, "#{name} must be integer but given #{value}" unless valid
  end
end
