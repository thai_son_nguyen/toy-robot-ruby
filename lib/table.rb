# frozen_string_literal: true

require_relative 'invalid_argument_exception'

class Table
  attr_reader :width, :height

  def initialize(width, height)
    guarantee_valid_dimension('width', width)
    guarantee_valid_dimension('height', height)
    @width = width
    @height = height
  end

  def occupiable?(coordinate)
    !coordinate.nil? && inbound?(0, width - 1, coordinate.x) && inbound?(0, height - 1, coordinate.y)
  end

  private

  def guarantee_valid_dimension(name, value)
    valid = (value.is_a? Integer) && value.positive?
    raise InvalidArgumentException, "#{name} must be positive integer but given #{value}" unless valid
  end

  def inbound?(min, max, value)
    min <= value && value <= max
  end
end
