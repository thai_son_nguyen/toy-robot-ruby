# frozen_string_literal: true

class Robot
  attr_reader :table, :coordinate, :direction

  def initialize(table)
    @table = table
  end

  def move_to(coordinate, direction)
    return unless table.occupiable?(coordinate)
    @coordinate = coordinate
    @direction = direction
  end

  def on_table?
    !coordinate.nil? && !direction.nil?
  end
end
