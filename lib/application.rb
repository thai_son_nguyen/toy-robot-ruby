# frozen_string_literal: true

require_relative 'table'
require_relative 'robot'
require_relative 'command_parser'

class Application
  class << self
    def run
      robot = Robot.new(Table.new(5, 5))
      play_robot(robot)
    end

    private

    def play_robot(robot)
      welcome
      while (command_string = ask_for_command)
        return if /^EXIT\z/i.match?(command_string)
        execute_command(command_string, robot)
      end
    end

    def ask_for_command
      print '> '
      gets.chomp
    end

    def execute_command(command_string, robot)
      CommandParser.parse(command_string).execute(robot)
    end

    def welcome
      puts <<~MSG
        Welcome to robot simulator !
        1. PLACE X,Y,F -> To place robot and start (with F = NORTH|SOUTH|WEST|EAST).
        2. MOVE -> To move robot forward.
        3. LEFT -> To turn left.
        4. RIGHT -> To turn right.
        5. REPORT -> To report location.
        6. EXIT or empty line -> To quit program.
      MSG
    end
  end
end
