##1. SYSTEM REQUIREMENTS
	1.1 Tested in ruby 2.5.1
    1.2 Development environment is MacOS

##2. RUN INSTRUCTIONS
	2.1 Clone the repo
	2.2 Run `bundle install`
    2.3 Execute `bin/run`

##3. DESIGN CHOICES
	3.1 Console command line input is used.
	3.2 Commands are not case-sentitive
	3.3 EXIT command is added to terminate program for convenience.
	3.4 REPORT command outputs data to console in standard format {x},{y},{direction}
	3.5 Invalid commands will be ignored.
    3.6 `rspec` is selected for BDD.

##4. DESIGN PRINCIPLES
	4.1 Command Design Pattern to separate logics to execute different commands
	4.2 S.O.L.I.D
	4.3 Test Driven Development.

##5. TEST STEPS
	PLACE 5,5,EAST			(Should be ignored)
	MOVE				    (Should be ignored)
	LEFT				    (Should be ignored)
	RIGHT				    (Should be ignored)
	REPORT				    (Should be ignored)

	PLACE 2,0,NORTH
	MOVE
	MOVE
	MOVE
	MOVE
	MOVE				    (Should be ignored)
	REPORT				    (Should output 2,4,NORTH)

	LEFT
	REPORT				    (Should output 2,4,WEST)
	MOVE
	MOVE
	MOVE				    (Should be ignored)
	REPORT				    (Should output 0,4,WEST)

	LEFT
	REPORT				    (Should output 0,4,SOUTH)
	MOVE
	MOVE
	MOVE
	MOVE
	MOVE				    (Should be ignored)
	REPORT				    (Should output 0,0,SOUTH)

	LEFT
	REPORT				    (Should output 0,0,EAST)
	MOVE
	MOVE
	MOVE
	MOVE
	MOVE				    (Should be ignored)
	REPORT				    (Should output 4,0,EAST)

	LEFT
	REPORT				    (Should output 4,0,NORTH)
	MOVE			
	REPORT				    (Should output 4,1,NORTH)

	RIGHT
	REPORT				    (Should output 4,1,EAST)
	RIGHT
	REPORT				    (Should output 4,1,SOUTH)
	RIGHT
	REPORT				    (Should output 4,1,WEST)

	PLACE 2,2,EAST
	REPORT				    (Should output 2,2,EAST)

	PLACE 1,1,WEST
	REPORT				    (Should output 1,1,WEST)

	PLACE 0,0,SOUTH
	REPORT				    (Should output 0,0,SOUTH)

	LEFT
	MOVE
	RIGHT
	REPORT				    (Should output 1,0,SOUTH)

	PLACE 2,2,SOUTH
	MOVE2				    (Should be ignored)
	LEFT2				    (Should be ignored)
	REPORT				    (Should output 2,2,SOUTH)

	RIGHT2				    (Should be ignored)
	REPORT				    (Should output 2,2,SOUTH)
	REPORT2				    (Should be ignored)

	EXIT				    (Should end program)
